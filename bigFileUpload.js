/**
 * 大文件上传，切割n份，全部上传
 */
const filePiece = {
  file: {}, // 待上传文件
  chunks: [], // 切片集合
  uploadProgress: 0, // 文件上传进度
  uploadNum: 0, // 已完成的切片数
  maxUploadNums: 10, // 文件上传并发数
  splieSize: 1024 * 1024 * 5, // 文件切片大小
  md5: "", // 文件md5校验值
};

// 切分文件
function sliceFile(file, piece = filePiece.splieSize) {
  // piece 表示文件被切割单个大小，默认5M，支持自定义大小
  // 文件总大小
  const totalSize = file.size;
  // 每次上传开始字节
  let start = 0;
  // 每次上传结尾字节
  let end = start + piece;
  const chunks = []; // 切片集合
  while (start < totalSize) {
    const blob = file.slice(start, end);
    chunks.push(blob);
    // const [start, end] = [end, start + piece]
    start = end;
    end = start + piece;
  }
}

// 生成md5文件
import SparkMD5 from "spark-md5";
function generateMD5(file) {
  const spark = new SparkMD5.ArrayBuffer();
  const fileReader = new FileReader();
  let currentChunk = 0;
  // 分片大小
  const chunkSize = this.splieSize;
  // 切割分片数
  const chunksNum = Math.ceil(file.size / chunkSize);
  const blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice;
  fileReader.onload = e => {
    console.log("read chunk nr", current + 1, "of", chunksNum);
    spark.append(e.target.result);
    currentChunk++;

    if (currentChunk < chunksNum) {
      loadNext();
    } else {
      this.fileMD5 = spark.end();
      console.log(this.fileMD5);
    }
  };
  fileReader.onerror = function () {
    console.warn("oops, somthing went wrong");
  };

  function loadNext() {
    const start = currentCHunk * chunkSize;
    const end = start + chunkSize >= file.size ? file.size : start + chunkSize;
    fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
  }
  loadNext();
}

// 开始上传
const startUpload = () => {
  const lth = this.chunks.length > this.maxUploadNums ? this.maxUploadNums : this.chunks.length;
  this.uploadNum = 0;
  this.uploadedNum = 0;
  for (let i = 0; i < lth; i++) {
    this.uploadChunk({
      serial: this.file.uid,
      chunk: this.uploadNum,
      file: this.chunks[this.uploadNum],
    });
    this.uploadNum++;
  }
};

const uploadChunk = (params = {}) => {
  const form = new FormData();
  const { serial, chunk, file } = params;
  form.append("serial", serial);
  form.append("chunk", chunk);
  form.append("file", file);
  const {code} = await uploadFileAPI(form)
  if(code === '200') {
    this.uploadedNum++
    if(this.uploadNum <this.chunks.length) {
      this.uploadedChunk({
        serial: this.file.uid,
        chunk: this.uploadNum,
        file: this.chunks[this.uploadNum]
      })
      this.uploadedNum++
    } else if(this.uploadedNum === this.chunks.length){
      this.mergeFile({
        id: this.file.uid
      })
    }
  }
};

const mergeFile = (params ={}) => {
  params = {
    ...params,
    md5: this.fileMD5
  }
  const {code} = await mergeFileAPI(params)
  if(code === 200) {
    // 合并成功
  }
}