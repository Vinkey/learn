// debounce

import { nullLiteral } from "babel-types";
import { useEffect, useLayoutEffect, useReducer, useState } from "react";
import { getSystemErrorMap } from "util";

// 多次触发，只执行一次
function debounce(fn, delay) {
  let timer;
  return function (...args) {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, args);
    }, delay);
  };
}

function task() {
  console.log("run task");
}

const debounceTask = debounce(task, 2000);

// 节流
// 规定时间内只执行一次
function throttle(fn, delay) {
  let last = 0;
  return function (...args) {
    let now = Date.now();
    if (now - last > delay) {
      last = now;
      fn.apply(this, args);
    }
  };
}
const throttleTask = throttle(fn, 2000);

// 深拷贝
/**
1、如果obj里面存在时间对象,JSON.parse(JSON.stringify(obj))之后，时间对象变成了字符串。
2、如果obj里有RegExp、Error对象，则序列化的结果将只得到空对象。
3、如果obj里有函数，undefined，则序列化的结果会把函数， undefined丢失。
4、如果obj里有NaN、Infinity和-Infinity，则序列化的结果会变成null。
5、JSON.stringify()只能序列化对象的可枚举的自有属性。如果obj中的对象是有构造函数生成的， 则使用JSON.parse(JSON.stringify(obj))深拷贝后，会丢弃对象的constructor。
6、如果对象中存在循环引用的情况也无法正确实现深拷贝。
 */
// 不支持值为undefined，函数跟循环引用的情况
const cloneObj = JSON.parse(JSON.stringify(obj));
// 递归实现
function deepClone(obj, cache = new WeakMap()) {
  if (obj === null || typeof obj !== "object") return obj;
  if (obj instanceof Date) {
    return new Date(obj);
  }
  if (obj instanceof RegExp) {
    return new RegExp(obj);
  }
  // 如果出现循环引用，返回缓存对象，防止递归进入死循环
  if (cache.has(obj)) {
    return cache.get(obj);
  }
  // 使用对象所属的构造函数创建新对象
  let cloneObj = new obj.constructor();
  cache.set(obj, cloneObj);

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      cloneObj[key] = deepClone(obj[key], cache); // 递归拷贝
    }
  }
  return cloneObj;
}
// test
const obj = {
  name: "jack",
  address: {
    x: 100,
    y: 200,
  },
};
obj.a = obj; // 循环引用
const newObj = deepClone(obj);
console.log(newObj.address === obj.address); // false

/** promise */
class MyPromise {
  constructor(executor) {
    this.status = "pending";
    this.value = null;
    this.reason = null;
    this.onFulFilledCallbacks = [];
    this.obRejectedCallbacks = [];

    let resolve = value => {
      if (this.value === "pending") {
        this.status = "fulfilled";
        this.value = value;
        this.onFulFilledCallbacks.forEach(fn => fn()); // 调用成功的回调函数
      }
    };

    let reject = reason => {
      if (this.status == "pending") {
        this.status = "rejected";
        this.reason = reason;

        // 调用失败的回调函数
        this.onRejectedCallbacks.forEach(fn => fn());
      }
    };

    try {
      executor(resolve, reject);
    } catch (err) {
      reject(err);
    }
  }
  then(onFulfilled, onRejected) {
    return new MyPromise((resolve, reject) => {
      if (this.status === "fulfilled") {
        setTimeout(() => {
          const x = onFulfilled(this.value);
          x instanceof MyPromise ? x.then(resolve, reject) : resolve(x);
        });
      }
      if (this.status === "rejected") {
        setTimeout(() => {
          const x = onRejected(this.reason);
          x instanceof MyPromise ? x.then(resolve, reject) : reject(x);
        });
      }
      if (this.status === "pending") {
        // 将成功的回调函数放入成功数组
        this.onFulFilledCallbacks.push(() => {
          setTimeout(() => {
            const x = onFulfilled(this.value);
            x instanceof MyPromise ? x.then(resolve, reject) : resolve(x);
          });
        });
        this.onRejectedCallbacks.push(() => {
          // 将失败的回调函数放入失败数组
          setTimeout(() => {
            const x = onRejected(this.reason);
            x instanceof MyPromise ? x.then(resolve, reject) : reject(x);
          });
        });
      }
    });
  }
}

/**
 *
 * @returns Promise.all
 */
Promise.myAll = function (promises) {
  return new Promise((resolve, reject) => {
    // promises 可以不是数组，但必须要有Iterator接口
    if (typeof promises[Symbol.iterator] !== "function") {
      reject("typeError: promises is not iterable");
    }
    if (promises.length) {
      resolve([]);
    } else {
      const res = [];
      const len = promises.length;
      let count = 0;
      for (let i = 0; i < len; i++) {
        // Promise.resolve的作用是将普通值或thenable对象转为promise
        Promise.resolve(promises[i]).then(data => {
          res[i] = data;
          count += 1;

          if (count === len) {
            resolve(res);
          }
        });
      }
    }
  });
};

// test case
function p1() {
  return new MyPromise((resolve, reject) => {
    setTimeout(resolve, 1000, 1);
  });
}
function p2() {
  return new MyPromise((resolve, reject) => {
    setTimeout(resolve, 1000, 2);
  });
}
p1()
  .then(res => {
    console.log(res); // 1
    return p2();
  })
  .then(ret => {
    console.log(ret); // 2
  });

// 异步控制并发数
function limitRequest(urls = [], limit = 3) {
  return new Promise((resolve, reject) => {
    const len = urls.length;
    let count = 0;

    // 同时启动limit个任务
    while (limit > 0) {
      start();
      limit -= 1;
    }

    function start() {
      //从数组中拿走第一个任务
      const url = urls.shift();
      if (url) {
        axios
          .post(url)
          .then(res => {
            // todo
          })
          .catch(err => {
            // todo
          })
          .finally(() => {
            if (count === len - 1) {
              // 最后一个任务完成
              resolve();
            } else {
              count++;
              start();
            }
          });
      }
    }
  });
}

// 测试
limitRequest(["http://xxa", "http://xxb", "http://xxc", "http://xxd", "http://xxe"]);

// 继承

class Parent {
  constructor(name) {
    this.name = name;
  }
  eat() {
    console.log(this.name + " is eating");
  }
}

class Child extends Parent {
  constructor(name, age) {
    super(name);
    this.age = age;
  }
}

let xm = new Child("xiaomin", 32);
xm.eat();
console.log(xm.name, "222");
console.log(xm.age, "333");

// useReducer
const reducer = (state, action) => {
  switch (action.type) {
    case "increasement":
      return { count: state.count + 1 };
    case "decreasement":
      return { count: state.count - 1 };
    default:
      return false;
  }
};

// useReducer只是useState的替代方案，适用于复杂状态
// useReducer只是单个组件的状态管理，组件通讯还需要依赖props，
// redux全局状态管理，多组件共享数据
const [state, dispatch] = useReducer(reducer, initalState);

const Fiber = {
  type: FunctionComponent,
  memorizedState: null,
};

function FunctionComponent() {
  const [count, setCount] = useReducer(x => x + 1, 0);
  return { count, setCount };
}

function useReducer(reducer, initalState) {
  if (!Fiber.memorizedState) Fiber.memorizedState = initalState;

  const dispatch = dispatchEvent.bind(null, Fiber, reducer);
  return [Fiber.memorizedState, dispatch];
}

function dispatchEvent(fiber, reducer, action) {
  reducer ? reducer(fiber.memorizedState) : action;
  fiber.type();
}

// hook的中间变量， 可以理解为正在工作的hook或尾hook
let workInProgressHook = null;
// 获取/创建hook的函数
function updateWorkInProgresshook() {
  // 获取旧fiber
  const current = Fiber.alternate;
  let hook;
  if (current) {
    // 如果存在旧fiber，则是更新阶段
    Fiber.memorizedState = current.memorizedState;
    if (workInProgressHook) {
      // 有尾部hook说明不是头节点
      hook = workInProgressHook = workInProgressHook.next;
    } else {
      hook = workInProgressHook = current.memorizedState;
    }
  } else {
    hook = {
      memorizedState: null,
      next: null,
    };
    if (workInProgressHook) {
      // 有尾部hook说明不是头节点
      workInProgressHook = workInProgressHook.next = hook;
    } else {
      workInProgressHook = Fiber.memorizedState = hook;
    }
  }

  return hook;
}

Fiber.memorizedState = {
  memorizedState: count1,
  next: {
    memorizedState: count2,
    next: {
      memorizedState: count3,
    },
  },
};

// useEffect \ useLayoutEffect
// Fiber节点
const Fiber = {
  type: FunctionComponent, // fiber节点上的type属性是组件函数
  effect: [],
  layoutEffect: [],
};

// 函数组件
function FunctionComponent() {
  useEffect(() => {
    console.log("effect");
  });
  useLayoutEffect(() => {
    console.log("useLayoutEffect");
  });
}

function useEffect(create) {
  Fiber.effect.push(create);
}

function useEffect(create) {
  Fiber.layoutEffect.push(create);
}
// 执行函数组件的渲染
Fiber.type();

// 渲染完成后的处理
for (let i = 0; i < Fiber.layoutEffect.length; i++) {
  const create = Fiber.layoutEffect[i];
  create();
}

for (let i = 0; i < Fiber.effect.length; i++) {
  const create = Fiber.effect[i];
  // useEffect的回调函数进行异步回调执行
  setTimeout(() => {
    create();
  });
}

// vue hooks
const App = {
  setup() {},
  render() {},
};

const FunctionComponent = (props, context) => {};

/**
 * React 的调度任务为什么选择使用 MessageChannel 实 现
首先是为了产生宏任务，因为只要宏任务才可以做到让出当前的主线程，交还给浏览器执行更新页面的任务，在浏览器执行完更新页面之后，可以继续执行未完成的任务。而微任务是在当前任务执行的最后执行的，
而且需要执行完当前执行栈产生的所有微任务才会把主线程让给浏览器，这样就做不到 React 需要实现的效果了，每执行一个更新任务，需要把这个更新任务更新的页面内容呈现给用户之后，再进行下一个更新任务。

为什么不使用 setTimeout 呢？因为 setTimeout(fn, 0) 默认就会存在 4ms 的延迟，在追求极致性能的 React 团队来说，这是不可接受的。

为什么不使用 requestAnimationFrame 呢？因为 requestAnimationFrame 是一帧只执行一次。这是什么概念呢？主流浏览器刷新频率为60Hz，即每（1000ms / 60Hz）16.6ms 浏览器刷新一次。
也就是说使用 requestAnimationFrame 的话，就可能会产生 16.6ms 的延迟，这比 setTimeout 的默认 4ms 的延迟造成的性能浪费更大了。
有 4ms 延迟的 setTimeout，React 团队都不能接受，那么高达 16.6ms 延迟的 requestAnimationFrame，React 团队更不可能接受了。另外 requestAnimationFrame 也存在兼容性问题，所以更不可能使用 requestAnimationFrame 了。

在 Vue3 中是没有宏任务的，Vue3 的异步任务，例如：nextTick 是一个微任务，所以使用 nextTick 是没办法达到我们想要的效果的，同时为了还原跟 React 一样，我们同样使用 MessageChannel 来进行 useEffect 的异步回调。
 */

// MessageChannel
function deepClone(obj) {
  return new Promise((resolve, reject) => {
    try {
      const { port1, port2 } = new MessageChannel();
      port2.onmessage = function (e) {
        resolve(e.data);
      };
      port1.postMessage(obj);
    } catch (error) {
      reject(error);
    }
  });
}

const obj = {
  a: {
    b: 1,
  },
};
deepClone(obj).then(newObj => {
  console.log(newObj === obj);
  newObj.a.b = 2;
  console.log(obj, "---", newObj);
});

// set 存储不重复的值的有序列表，可以foreach遍历，WeakSet不接收基础数据类型，只接受对象,不暴露迭代器不可循环，没有size属性，属于弱饮用，可被对象实例回收。
var set = new Set();
set.add(1);
set.add("2");

set.delete(1);

var set = new WeakSet();
let key = { a: "1" };
set.add(key);
set.delete(key);

// 微前端--
// 多个子应用集成在一个主应用下，通过主应用来加载这些子应用。
// 一般基于single-spa来实现，single-spa缺点：样式不隔离，没有js沙箱机制
// 乾坤无界之类，处理了css隔离沙箱（动态样式表，加载新应用时删除老应用的style，or利用css module，打包时生成不冲突的选择器名-当前应用的模块名）
// js沙箱用proxy代理沙箱，不影响全局环境or记录快照，切换时依据快照恢复环境。
var a = [];
function getSumIndex(nums, target) {
  let map = {};
  if (Array.isArray(nums) && nums.length > 2) {
    for (let i = 0; i < nums.length; i++) {
      for (let j = i + 1; j < nums.length; j++) {
        if (nums[i] + nums[j] === target) {
          nums[i] < nums[j]
            ? (map[nums[i]] = [nums[i], nums[j]])
            : (map[nums[j]] = [nums[j], nums[i]]);
          console.log(map);
          a = Object.values(map);
        }
      }
    }
  }
}

// vue3相比vue2diff算法
vue2 updatechildren采用双端比较
vue3增加了最长递增子序列
新增patchFlag, 静态提升，函数缓存

vue响应式原理
reactivity函数
Object.defineProperty Proxy getter settet 方法

port 端口 http 80 https 443

户名，卡号，开户行，分行，分行地址省市区，银行卡照片 ---- 不同药企form表单项不同，formConfig配置化思维处理